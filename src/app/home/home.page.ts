import { Component } from '@angular/core';
import { PhotoService } from '../services/photo.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private photoService: PhotoService, public toastController: ToastController) { }
  sending: string = ""

  async presentToast(res) {
      const toast = await this.toastController.create({
        message: 'Result: ' + res.Result + ' |  Confidence: ' + res.Confidence,
        duration: 2000
      });
      toast.present();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: 'Click to Close',
      icon: 'information-circle',
      position: 'top',
      buttons: [
        {
          side: 'start',
          icon: 'star',
          text: 'Favorite',
          handler: () => {
            console.log('Favorite clicked');
          }
        }, {
          text: 'Done',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  TakePicture = () => {
    this.photoService.takePicture()
  }
  imageSend = () => {
    this.sending = "Sending to http://54.255.238.225:5000/"
    this.photoService.sendImage().subscribe(res => {
      this.presentToast(res)
    })
  }
}
