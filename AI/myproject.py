
from flask import Flask, jsonify, request
from flask_cors import CORS

import base64
from io import BytesIO
from PIL import Image

import torch
import torchvision
import torchvision.transforms as transforms
import torchvision.transforms.functional as fn
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

batch_size = 4

trainset = torchvision.datasets.CIFAR10(root='./datasets', train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='./datasets', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
                                         shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')


def base64_pil(base64_str):
    image = base64.b64decode(base64_str)
    image = BytesIO(image)
    image = Image.open(image).convert('RGB')
    return image

def imshow(img):
    img = img / 2 + 0.5    
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

def predict(input):
    out = net(input.reshape(1, 3, 32, 32))
    _, pred = torch.max(out, dim=1)
    prob = torch.nn.functional.softmax(out, dim=1)
    conf, classe = torch.max(prob, 1)
    return classes[pred], float(conf.data[0])

def check_sample(img_label_pair):
    return predict(img_label_pair[0])


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

net = Net().to("cpu")
net.load_state_dict(torch.load('./cifar_net.pth', map_location="cpu"))
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

@app.route("/")
def Hello():
    return "CZ4171 Iskandar Project"

@app.route("/predict", methods=['POST'])
def Predict():
    data = request.json
    img = base64_pil(data['image'])
    inputs = transform(img)
    inputs = fn.resize(inputs, size=[32,32])
    inputs = inputs.reshape(1, 3, 32, 32)
    pred, conf = check_sample(inputs)
    res = { 'Result' : pred , 'Confidence': str(conf * 100) }
    return jsonify(res)


if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0")
