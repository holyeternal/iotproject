#imports
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np

# TO DO: Create Simple CNN 
class CNN(nn.Module):
    def __init__(self, in_channels = 3, num_classes = 10):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self,x):
        x = self.pool(F.relu(self.conv1(x))) #ReLu make negative value to 0
        x = self.pool(F.relu(self.conv2(x)))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

def save_checkpoint(state, epocj):
    print("Saving Checkpoint, at epoch:" + epocj)
    PATH= './cifar_net.pth.tar'
    torch.save(state,PATH)


#Set device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)

#Hyper Parameter
in_channels = 1
num_classes = 10
learning_rate = 0.001
batch_size = 64
num_epochs = 2

#Load Data
train_dataset = datasets.CIFAR10(root='datasets/', train=True, transform=transforms.ToTensor(), download=True) #Training Set | Download to true is file doesn't exist
train_loader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)

test_dataset = datasets.CIFAR10(root='datasets/', train=False, transform=transforms.ToTensor(), download=True) #Test Set | Download to true is file doesn't exist
test_loader = DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=True)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

#Initialize Network
model = CNN().to(device)

#Loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

#Train Network
# for epoch in range(num_epochs): #1 Epoch = 1 Cycle of seeing all images in the network

#     checkpoint = {'state_dict': model.state_dict(), optimizer: optimizer.state_dict() }
#     save_checkpoint(checkpoint, epoch)

#     for batch_idx , (data, targets) in enumerate(train_loader):
#         #Get data to CUDA if possible
#         data = data.to(device=device)
#         targets = targets.to(device=device)

#         #torch.Size([64, 1, 28, 28])
#         #64 = number of image
#         #28 = dimension of image

#         #flatten dimensions
#         #Get to correct shape
#         #data = data.reshape(data.shape[0], -1) Since its already flatten in in forward 

#         #Forward
#         scores = model(data)
#         loss = criterion(scores, targets)

#         #Backward
#         optimizer.zero_grad() #Set all gradient to 0
#         loss.backward()

#         #Gradient descent or adam step
#         optimizer.step()


# #Check accuracy on training and test to see how good our model
# def check_accuracy(loader,model):
#     if loader.dataset.train:
#         print("Checking accuracy on training data")
#     else:
#         print("Checking accuracy on test data")
#     num_correct = 0
#     num_samples = 0
#     model.eval()

#     with torch.no_grad():
#         for x, y in loader:
#             x = x.to(device=device)
#             y = y.to(device=device)
#             #x = x.reshape(x.shape[0],-1)

#             scores = model(x) #64 x 10
#             _, predictions = scores.max(1)
#             num_correct += (predictions == y).sum()
#             num_samples += predictions.size(0)
        
#         print(f'Got {num_correct} / {num_samples} with accuracy {float(num_correct) / float(num_samples)*100:.2f}')

#     model.train()
#     return float(num_correct) / float(num_samples)*100


# check_accuracy(train_loader, model)
# check_accuracy(test_loader, model)

