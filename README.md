
# U1922414E Aide Iskandarr NTU Project
AI folder is the python flask project

## Self-Train Accuracy
Using Cifar 10

Accuracy for class: plane is 47.7 %
Accuracy for class: car   is 82.3 %
Accuracy for class: bird  is 40.3 %
Accuracy for class: cat   is 43.8 %
Accuracy for class: deer  is 43.4 %
Accuracy for class: dog   is 32.8 %
Accuracy for class: frog  is 62.7 %
Accuracy for class: horse is 54.6 %
Accuracy for class: ship  is 54.9 %
Accuracy for class: truck is 49.9 %

## Hardware used
Ryzen 9 3900x (12 cores)
G-Skill Trident Z Neo 3600Mhz 64GB
MSI RTX 3090 Gaming X Trio (24GB)

## License
[MIT](https://choosealicense.com/licenses/mit/)